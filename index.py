from flask import Flask, render_template, request, redirect, url_for,session
import mysql.connector


app = Flask(__name__)


app.secret_key =  b'_5#y2L"F4Q8z\n\xec]/'

def ingresar(email, clave):
    conn = mysql.connector.connect(
        host = "127.0.0.1",
        user = "root",
        password="",
        database="login"
    )
    
    sql = "select email, perfil from logins where email = %s and clave = %s"

    #conn = mysql.connection
    cur = conn.cursor()
    cur.execute(sql, (email, clave))
    usuario = cur.fetchone()

    return usuario


@app.route("/", methods=["GET", "POST"])
def home():
    if request.method == "POST":

        res = ingresar(request.form["email"], request.form["clave"])
        session["perfil"] = res[1]
      
        return redirect(url_for("pagina1"))
        """
        if res[1] == "user":
            return redirect(url_for("user_home"))
        elif res[1] == "admin":
            
            return redirect(url_for("pagina1"))
        """

    else:
        return render_template("home.html")


@app.route("/user/home")
def user_home():
    return render_template("user.html")

@app.route("/admin/home")
def admin_home():
    return render_template("admin.html")

@app.route("/pagina1")
def pagina1():
    return render_template("pagina1.html")

app.run(debug=True)